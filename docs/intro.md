---
hide:
  - toc        # Hide table of contents
  - navigation
---

# Introduction

!!! abstract "Présentation"


    L’enseignement de sciences numériques et technologie en classe de seconde a pour objet de permettre d’appréhender les principaux concepts des sciences numériques, mais également de permettre aux élèves, à partir d’un objet technologique, de comprendre le poids croissant du numérique et les enjeux qui en découlent. La numérisation généralisée des données, les nouvelles modalités de traitement ou de stockage et le développement récent d’algorithmes permettant de traiter de très grands volumes de données numériques constituent une réelle rupture dans la diffusion des technologies de l’information et de la communication. Cette révolution multiplie les impacts majeurs sur les pratiques humaines.

    Thématiques du programme

    - Internet
    - Le Web
    - Les réseaux sociaux
    - Les données structurées et leur traitement
    - Localisation, cartographie et mobilité
    - Informatique embarquée et objets connectés
    - La photographie numérique

??? note "Programme de sciences numériques et technologie de seconde générale et technologique"

	<div class="centre">
	<iframe 
	src="../documents/BO.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>
